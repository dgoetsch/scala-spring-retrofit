package com.devyn.scala.spring.retrofit

import com.fasterxml.jackson.databind.ObjectMapper
import org.scalatest.{FlatSpec, Matchers, BeforeAndAfterEach}
import org.springframework.boot.test.{IntegrationTest, SpringApplicationContextLoader}
import org.springframework.test.context.{TestContextManager, ContextConfiguration}
import org.springframework.test.context.web.WebAppConfiguration
import retrofit.{JacksonConverterFactory, Retrofit}

@ContextConfiguration(
  classes = Array(classOf[Application]),
  loader = classOf[SpringApplicationContextLoader])
@WebAppConfiguration
@IntegrationTest(Array("server.port=9011"))
class IntegrationSpec extends FlatSpec with Matchers with BeforeAndAfterEach {

  var client: Client = _
  new TestContextManager(this.getClass()).prepareTestInstance(this)

  override def beforeEach(): Unit = {
    client = new Retrofit.Builder()
      .baseUrl("http://localhost:9011")
      .addConverterFactory(JacksonConverterFactory.create(new ObjectMapper()))
      .build()
      .create(classOf[Client]);
  }

  "the client"  should "fetch the correct object" in {
    val thing = client.getThing("aThing").execute().body()
    println( thing )
    assert(thing.id == "aThing")
    assert(thing.name == "a name")
  }
}
