package com.devyn.scala.spring.retrofit

import org.springframework.web.bind.annotation.{RequestMethod, PathVariable, RestController, RequestMapping}

import scala.beans.BeanProperty

@RestController
@RequestMapping(Array("/things"))
class Controller {

  @RequestMapping(
    value = Array("/{thingId}"),
    method = Array(RequestMethod.GET),
    produces = Array("application/json; charset=utf-8"))
  def getThing(@PathVariable("thingId") id: String):  Thing = {
    new Thing("a name", id)
  }
}

class Thing(@BeanProperty var name: String, @BeanProperty var id: String) {
  def this() = this(null, null) //need default constructor because retrofit
}