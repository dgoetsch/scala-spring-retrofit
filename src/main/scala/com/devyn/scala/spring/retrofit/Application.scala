package com.devyn.scala.spring.retrofit

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class Application {

}

object Application {
  def main(args: Array[String]): Unit = {
    SpringApplication.run(Array(classOf[Application]: AnyRef), args)
  }
}