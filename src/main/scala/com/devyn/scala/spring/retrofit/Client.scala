package com.devyn.scala.spring.retrofit

import retrofit.Call
import retrofit.http.{Path, GET}

/**
  * Created by devyn on 11/21/15.
  */
trait Client {

  @GET("/things/{id}/")
  def getThing(@Path("id") id: String): Call[Thing]

}
