enablePlugins(JavaAppPackaging)

name := "spring-boot-scala-retrofit"

version := "0.1.0"

scalaVersion := "2.11.7"

sbtVersion := "0.13.9"

val springBootVersion = "1.3.0.RELEASE"
val jacksonVersion = "2.6.1"
val retrofitVersion = "2.0.0-beta2"
libraryDependencies ++= Seq(
  "org.springframework.boot" % "spring-boot-starter-web" % springBootVersion,
  "org.springframework.boot" % "spring-boot-starter-actuator" % springBootVersion,
  "org.springframework.boot" % "spring-boot-starter-test" % springBootVersion,
  "org.springframework.boot" % "spring-boot-starter-log4j2" % springBootVersion,
  "org.springframework.boot" % "spring-boot-starter-test" % springBootVersion % "test",
  "org.apache.logging.log4j" % "log4j-api" % "2.4",
  "org.apache.logging.log4j" % "log4j-core" % "2.4",
  "org.apache.logging.log4j" % "log4j-1.2-api" % "2.4",
  "org.apache.logging.log4j" % "log4j-jcl" % "2.4",
  "org.apache.logging.log4j" % "log4j-jul" % "2.4",
  "org.apache.logging.log4j" % "log4j-slf4j-impl" % "2.4",
  "com.fasterxml.jackson.module" % "jackson-module-scala_2.11" % jacksonVersion,
  "com.fasterxml.jackson.datatype" % "jackson-datatype-jsr310" % jacksonVersion,
  "com.fasterxml.jackson.dataformat" % "jackson-dataformat-yaml" % jacksonVersion,
  "com.squareup.retrofit" % "retrofit" % retrofitVersion,
  "com.squareup.retrofit" % "converter-jackson" % retrofitVersion,
  "org.mockito" % "mockito-core" % "1.10.19" % "test",
  "org.scalatest" % "scalatest_2.11" % "2.2.5" % "test"
)